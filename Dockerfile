FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN apt-get update && apt-get upgrade -y
RUN mkdir -p /app/code/spreed /app/code/site && \
	cd /app/code/spreed && \
	git clone https://github.com/strukturag/nextcloud-spreed-signaling.git . && \
	make build && \
	mkdir /app/code/nats
RUN cd /app/code/nats && \
	curl -L https://github.com/nats-io/nats-server/releases/download/v2.1.7/nats-server-v2.1.7-linux-amd64.zip -o nats-server.zip && \
	unzip nats-server.zip && \
	mv nats-server-v2.1.7-linux-amd64/* .
#RUN sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-janus.asc https://packaging.gitlab.io/janus/gpg.key && \
#	. /etc/lsb-release; echo "deb [arch=amd64] https://packaging.gitlab.io/janus/$DISTRIB_CODENAME $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/morph027-janus.list && \
#	apt update && \
#	apt install janus janus-tools -y
RUN apt install -y libmicrohttpd-dev libjansson-dev \
	libssl-dev libsrtp-dev libsofia-sip-ua-dev libglib2.0-dev \
	libopus-dev libogg-dev libcurl4-openssl-dev liblua5.3-dev \
	libconfig-dev pkg-config gengetopt libtool automake libgtk2.0-dev

RUN mkdir /app/code/libnice && \
	cd /app/code/libnice && \
	curl -L https://libnice.freedesktop.org/releases/libnice-0.1.17.tar.gz -o libnice-0.1.17.tar.gz && \
	tar -xvzf libnice-0.1.17.tar.gz && cd libnice-0.1.17 && \
	./configure && make && make install

RUN mkdir /app/code/libsrtp && \ 
	cd /app/code/libsrtp && \
	curl -L https://github.com/cisco/libsrtp/archive/v2.3.0.zip -o v2.3.0.zip && \
	unzip v2.3.0.zip && \
	cd libsrtp-2.3.0 && \
	./configure && make && make install

RUN mkdir /app/code/janus-gateway && \
	cd /app/code && \
	git clone https://github.com/meetecho/janus-gateway.git janus-gateway && \
	cd janus-gateway && \
	sh autogen.sh && \
	./configure && make && make install && make configs

RUN rm /etc/apache2/sites-enabled/* \
    && sed -e 's,^ErrorLog.*,ErrorLog "/dev/stderr",' -i /etc/apache2/apache2.conf \
    && sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf \
    && a2disconf other-vhosts-access-log && a2enmod rewrite env proxy proxy_http proxy_wstunnel \
    && echo "Listen 8000" > /etc/apache2/ports.conf 
COPY server.conf /app/code/spreed/server.conf 
COPY janus.jcfg /etc/janus/janus.jcfg 
COPY startup.sh /app/code/startup.sh 
COPY apache2.conf /etc/apache2/sites-available/app.conf 
COPY mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY index.html /app/code/site 
RUN a2ensite app
RUN chmod +x /app/code/startup.sh 
RUN chown -R www-data.www-data /app/code 
CMD [ "/app/code/startup.sh" ]
#don't forget to set the turn server in janus.jcfg it's also in server.conf
