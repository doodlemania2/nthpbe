#!/bin/bash

set -eu
echo 'Starting janus in foregroundish mode (there is a -b we should use at some point)'
janus &
sleep 5;
echo 'Startup nats in daemon mode'
/app/code/nats/nats-server &
sleep 5;
echo 'Starting signaling'
cd /app/code/spreed && ./bin/signaling &
sleep 5;
echo "Starting apache..."
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
